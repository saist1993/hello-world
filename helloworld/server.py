import argparse
import logging
from flask import Flask, json, request

app = Flask(__name__)  # Creates the flask app.


@app.route('/messages', methods=['POST'])
def api_message():
    """An example of handling simple post request with various content-type.
    :return:
        str: User message along with content-type
            for text/plain and application/json.
        Unsupported media type for any other content-type.
    """
    if request.headers['Content-Type'] == 'text/plain':
        return "Text Message: " + request.data

    elif request.headers['Content-Type'] == 'application/json':
        return "JSON Message: " + json.dumps(request.json)

    else:
        return "415 Unsupported Media Type ;)"


if __name__ == '__main__':
    # Setting up command line arguments
    parser = argparse.ArgumentParser(description='Changing defaults '
                                                 'of the web server.')
    parser.add_argument('port', type=int,
                        help='port number of the web server')
    parser.add_argument('url', type=str,
                        help='host name of the web server')
    args = parser.parse_args()

    # Basic logging
    logging.basicConfig(level=logging.INFO)

    # Running the server
    logging.info(f"Server is about to run on host {args.url}, "
                 f"and port {args.port}")
    app.run(debug=True, port=5000)
