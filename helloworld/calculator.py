class Calculate:

    def add(self, a: int, b: int):
        """adds two given number
        :param a: int first value to add
        :param b: int second value to add
        :return: int sum of a and b (a+b)
        """
        return a + b

    def subtract(self, a: int, b: int):
        """subtracts the two given number.
        :param a: int first value
        :param b: int second value
        :return: int difference between the first and second
        """
        return a - b
