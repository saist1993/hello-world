from helloworld import calculator


class TestCalculator:
    """Test the calculator class."""

    def test_add_simple(self):
        """Test for checking correct addition of ints"""
        calc = calculator.Calculate()
        output, expected_sum = calc.add(12, 16), 28
        assert expected_sum == output, f"Expected output was {expected_sum}," \
                                       f" but " \
                                       f"received {output}"

    def test_add_dtypes(self):
        """Tests the add functionality of the calculator class"""
        calc = calculator.Calculate()

        # Test for float and integer
        output, expected_sum = calc.add(12.0, 16), 28.0
        assert expected_sum == output, f"Expected output was {expected_sum}," \
                                       f" but " \
                                       f"received {output}. Failed on float " \
                                       f"+ integer"
        # Test for floats
        output, expected_sum = calc.add(12.0, 16.2), 28.2
        assert expected_sum == output, f"Expected output was {expected_sum}," \
                                       f" but " \
                                       f"received {output}. Failed on float " \
                                       f"+ float"

        # Test for float and None
        try:
            _ = calc.add(12.0, None)
        except TypeError:
            pass
        except Exception as e:
            raise AssertionError(f"expected Type error but received {e}")

    def test_add_infs(self):
        """Test the add functionality for infinity."""
        calc = calculator.Calculate()
        output, expected_sum = calc.add(float('inf'), 12), float("inf")
        assert output == expected_sum, f"Expected output was {expected_sum}," \
                                       f" but " \
                                       f"received {output}"
